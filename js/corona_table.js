let columns_replacements = {
    "Gemnr": "Gemeente nummer",
    "BevAant": "Aantal inwoners",
    "Meldingen": "Besmettingen"

}

let cors_url = "https://cors-anywhere.herokuapp.com/"
let rivm_url = cors_url + "https://www.rivm.nl/coronavirus-kaart-van-nederland-per-gemeente";
let corona_data = [];

function getCSV() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", rivm_url, true);
    xhr.responseType = "document";
    xhr.onload = () => {
        if (xhr.status == 200) {
            processRIVM(xhr.responseXML);
        } else {
            displayError("Could not get data from RIVM website: code " + xhr.status + ". Try refreshing the page");
        }
    };
    xhr.send();
}

function search() {
    let q = search_bar.value.toLowerCase();

    if (q == "") {
        render_corona_data(corona_data);
    } else {
        render_corona_data(
            corona_data.filter(element => {
                return element[1].toLowerCase().includes(q);
            })
        )
    }

}

function render_corona_data(data) {
    let table = document.createElement("tbody");
    data.forEach(entry => {
        let row = document.createElement("tr");
        entry.forEach(e => {
            let cell = document.createElement("td");
            cell.innerText = e;
            row.appendChild(cell);
        });
        table.appendChild(row);
    })
    full_table.innerHTML = table.innerHTML;
}

function processRIVM(data) {
    console.log("Loaded CSV");

    let modifyDate = data.getElementsByClassName("content-date-edited")[0].innerText;
    document.getElementById("date-changed").innerText = modifyDate;

    let csv = data.getElementById("csvData").innerText
        .split("\n").filter(el => el != "");

    let headers_html = generateHeader(csv[0].split(";"));
    table_header.innerHTML += headers_html.innerHTML;

    document.getElementById("loading-message").setAttribute("class", "d-none");

    csv.slice(1).forEach(entry => corona_data.push(entry.split(';')));
    render_corona_data(corona_data);
    sorttable.makeSortable(document.getElementById('corona-table'));

    console.log("Parsed CSV")

}

function displayError(message) {
    document.getElementById("error_message").innerHTML = message;
    document.getElementById("error-box").setAttribute("style", "");
}

function closeError() {
    document.getElementById("error-box").setAttribute("style", "display: none;");
}

function generateHeader(headers) {
    let ans = document.createElement("thead");
    headers.forEach(header => ans.appendChild(headerToHTML(header)));
    return ans;
}

function headerToHTML(header_name) {
    let header = document.createElement("th");
    header.setAttribute("scope", "col");

    if (header != "Gemeente")
        header.setAttribute("class", "sorttable_numeric");

    let replacement = columns_replacements[header_name];

    header.innerText = replacement ? replacement : header_name;
    return header
}